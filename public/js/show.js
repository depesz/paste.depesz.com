window.onload = function(){

    // Handle switching tabs (original/pretty)
    var all_tabs = document.querySelectorAll('#displayMenu li');
    var all_bodies = document.querySelectorAll('div.display-option');
    function show_tab( this_id ) {
        if ( ! this_id.startsWith('dm-') ) {
            return
        }
        var body_id = 'display-' + this_id.substring(3);
        for (var i = 0; i < all_tabs.length; i++) {
            if ( all_tabs[i].id == this_id ) {
                all_tabs[i].classList.remove('inactive');
                all_tabs[i].classList.add('active');
            } else {
                all_tabs[i].classList.remove('active');
                all_tabs[i].classList.add('inactive');
            }
        }
        for (var i = 0; i < all_bodies.length; i++) {
            if (all_bodies[i].id == body_id) {
                all_bodies[i].style.display='block';
            } else {
                all_bodies[i].style.display='none';
            }
        }
    }
    for (var i = 0; i < all_tabs.length; i++) {
        all_tabs[i].addEventListener('click', function() { show_tab(this.id); });
    };
    show_tab('dm-original');

    // Enable syntax highlighting
    var pres = document.getElementsByTagName('pre');
    for (var i = 0; i < pres.length; i++ ) {
        hljs.highlightBlock(pres[i]);
        hljs.lineNumbersBlock(pres[i]);
    }

    // Make "copy … to clipboard" buttons work
    for (const button of document.querySelectorAll('button.copy')) {
        button.onclick = function(e) {
            var source_id = e.target.dataset.src;
            var source = document.getElementById(source_id).textContent;
            navigator.clipboard.writeText( source );
            var text_rollback = e.target.textContent;
            e.target.textContent = 'Copied …';
            setTimeout(() => {
                e.target.textContent = text_rollback;
            }, "2000");
        };
    }

    // Make "Edit as new" buttons work
    for (const button of document.querySelectorAll('button.editnew') ) {
        console.log(button);
        console.log(button.dataset.id);
    }

};
