window.onload = function(){

    // Handle calculation of proof-of-work to reduce spam
    var form  = document.getElementById('new-paste');
    var paste = document.getElementById('paste');
    var fisk  = document.getElementById('fisk');
    form.addEventListener('submit', function() {
        fisk.value = md5( paste.value.replace(/\s+/g, '') );
    });

    // Handle of auto-hint fields
    var ahs = document.getElementsByClassName('auto-hint');
    for (var i = 0; i < ahs.length; i++ ) {
        var e = ahs[i];
        if (e.value == '') {
            e.value = e["title"];
        }
        e.addEventListener( 'focus', function() {
            if ( this.value == this["title"] ) {
                this.value = '';
            }
        });
        e.addEventListener( 'blur', function() {
            if ( this.value == '' ) {
                this.value = this["title"];
            }
        });
    }

    // ctrl+enter in paste box should submit the paste. Idea by ilmari @ irc.
    console.log("form:", form);
    console.log("paste:", paste);
    paste.addEventListener( 'keyup', function(e) {
        if ( e.ctrlKey && e.key == "Enter" ) {
            fisk.value = md5( paste.value.replace(/\s+/g, '') );
            form.submit();
        }
    });
};
