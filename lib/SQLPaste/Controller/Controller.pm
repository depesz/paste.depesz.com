package SQLPaste::Controller::Controller;
use Mojo::Base 'Mojolicious::Controller';

use File::Temp  qw( tempfile );
use Digest::MD5 qw( md5_hex );
use Encode      qw( encode );
use English     qw( -no_match_vars );
use Data::Dumper;

use pgFormatter::Beautify;
use Pg::SQL::PrettyPrinter;

sub index {
    my $self = shift;
    $self->stash->{ 'default-public' } = 1;
    return $self->render();
}

sub new_paste {
    my $self = shift;

    # This field should be invisilbe, so if it's set, then I assume it was submit by spam-bot.
    return $self->redirect_to( 'home' ) if $self->req->param( 'website' );
    return $self->redirect_to( 'home' ) if $self->req->param( 'name' );
    return $self->redirect_to( 'home' ) if $self->req->param( 'email' );

    # This field should be set.
    return $self->redirect_to( 'home' ) unless $self->req->param( 'fisk' );

    my $paste = $self->req->param( 'paste' );
    return $self->redirect_to( 'home' ) unless $paste;
    if ( 10_000_000 < length $paste ) {
        $self->stash->{ 'message' } = 'Your paste is too long.';
        return $self->render( 'template' => 'controller/index' );
    }
    $paste =~ s/\r?\n/\n/g;

    my $paste_for_md5 = encode( "UTF-8", $paste );
    $paste_for_md5 =~ s/\s+//g;
    my $md5 = md5_hex( $paste_for_md5 );

    # This is also not visible but should have a value "fisk" - set by JS or manually in curl script
    if ( $self->req->param( 'fisk' ) ne $md5 ) {

        # $self->app->log->info( "Bad MD5, perl: $md5, js: " . $self->req->param('fisk') );
        return $self->redirect_to( 'home' );
    }

    my $title = $self->req->param( 'title' ) || undef;
    $title = undef if ( defined $title ) && ( $title eq 'Optional title' );

    if (
        ( !defined $title )
        && (
            $paste eq
q{SELECT n.nspname as "Schema", c.relname as "Name", CASE c.relkind WHEN 'r' THEN 'table' WHEN 'v' THEN 'view' WHEN 'm' THEN 'materialized view' WHEN 'i' THEN 'index' WHEN 'S' THEN 'sequence' WHEN 's' THEN 'special' WHEN 'f' THEN 'foreign table' WHEN 'P' THEN 'table' END as "Type", pg_catalog.pg_get_userbyid(c.relowner) as "Owner"
FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
WHERE c.relkind IN ('r', 'P','v','m','S','f','') AND n.nspname <> 'pg_catalog' AND n.nspname <> 'information_schema' AND n.nspname !~ '^pg_toast' AND pg_catalog.pg_table_is_visible(c.oid)
ORDER BY 1,2;}
           )
       )
    {
        return $self->redirect_to( 'show', id => 'e' );
    }

    my $public = $self->req->param( 'is_public' ) ? 1 : 0;
    my $expiry = $self->req->param( 'expiry' );
    $expiry = '1 day' unless $expiry =~ m{\A[1-9]\d* (day|week|month|year)s?\z};

    my $id = $self->database->save_paste( $title, $paste, $public, $expiry );
    $self->app->log->info( 'New paste saved with id: ' . $id );

    return $self->redirect_to( 'show', id => $id );
}

sub history {
    my $self = shift;

    my $raw_data = $self->database->get_history_data();
    my @by_day   = ();
    for my $row ( @{ $raw_data } ) {
        if (   ( 0 == scalar @by_day )
            || ( $row->[ 2 ] ne $by_day[ -1 ]->{ 'day' } ) )
        {
            push @by_day,
                {
                'day'    => $row->[ 2 ],
                'pastes' => [],
                };
        }
        push @{ $by_day[ -1 ]->{ 'pastes' } },
            {
            'id'    => $row->[ 0 ],
            'title' => $row->[ 1 ],
            };
    }
    $self->stash->{ 'history_by_day' } = \@by_day;
}

sub show {
    my $self = shift;

    my $id = defined $self->stash->{ id } ? $self->stash->{ id } : '';
    $self->redirect_to( 'home' ) if '' eq $id;

    my $paste = $self->database->get_paste_data( $id );
    $paste->{ 'orig' } = $paste->{ 'paste' };

    $self->prettify_paste( $paste );

    $self->stash->{ 'paste' } = $paste;
}

sub prettify_paste {
    my $self  = shift;
    my $paste = shift;
    my $id    = $paste->{ 'id' };

    my $pretty_printer = $self->app->home->rel_file( 'script/pg_query_beautify.rb' );

    if ( defined $paste->{ 'cached' } ) {
        $paste->{ 'pretty' } = $paste->{ 'cached' };
        return;
    }

    my $pretty;
    eval {
        my $pspp = Pg::SQL::PrettyPrinter->new(
            'service' => $self->app->config->{ 'parser-url' },
            'sql'     => $paste->{ 'paste' }
        );
        $pspp->parse();
        $pretty = join( ";\n\n", map { $_->pretty_print } @{ $pspp->{ 'statements' } } ) . ";\n";
        $pretty .= "-- Formatted by Pg::SQL::PrettyPrinter\n";
    };
    if ( $EVAL_ERROR ) {
        $self->app->log->error(
            "beautifying query failed: " . Dumper(
                {
                    'id'    => $id,
                    'query' => $paste->{ 'paste' },
                    'error' => $EVAL_ERROR->message
                }
            )
        );
        my $beautifier = pgFormatter::Beautify->new();
        $beautifier->query( $paste->{ 'paste' } );
        $beautifier->beautify();
        $pretty = $beautifier->content();
        $pretty .= "-- Formatted by pgFormatter::Beautify\n";
    }

    $paste->{ 'pretty' } = $pretty;
    $self->database->update_cache( $id, $paste->{ 'pretty' } );
    return;
}

sub edit_new {
    my $self = shift;

    my $id  = $self->req->param( 'id' )  // '';
    my $src = $self->req->param( 'src' ) // '';
    $self->redirect_to( 'home' ) if '' eq $id;
    $self->redirect_to( 'home' ) unless $src =~ m{\A[op]\z};

    my $paste = $self->database->get_paste_data( $id );
    my $title = $paste->{ 'title' } // '';
    my $text  = $paste->{ 'paste' };

    if ( $src eq 'p' ) {
        $self->prettify_paste( $paste );
        $text = $paste->{ 'pretty' };
    }

        $self->app->log->error(
            "the thing: " . Dumper(
                {
                    'object'    => $paste,
                    'force_title' => $title,
                    'force_paste' => $text,
                }
            )
        );

    $self->stash->{ 'force_title' } = $title;
    $self->stash->{ 'force_paste' }  = $text;
    return $self->render( 'template' => 'controller/index' );
}

sub prettify {
    my $self   = shift;
    my $query  = $self->req->param( 'q' );
    my $pretty = 'No query provided!';
    eval {
        my $pspp = Pg::SQL::PrettyPrinter->new(
            'service' => $self->app->config->{ 'parser-url' },
            'sql'     => $query,
        );
        $pspp->parse();
        $pretty = join( ";\n\n", map { $_->pretty_print } @{ $pspp->{ 'statements' } } ) . ";\n";
        $pretty .= "-- Formatted by Pg::SQL::PrettyPrinter\n";
    };
    if ( $EVAL_ERROR ) {
        $self->app->log->error(
            "beautifying query failed: " . Dumper(
                {
                    'id'    => 'one-shot',
                    'query' => $query,
                    'error' => $EVAL_ERROR->message
                }
            )
        );
        my $beautifier = pgFormatter::Beautify->new();
        $beautifier->query( $query );
        $beautifier->beautify();
        $pretty = $beautifier->content();
        $pretty .= "-- Formatted by pgFormatter::Beautify\n";
    }
    return $self->render( text => $pretty, format => 'txt' );
}

sub about {
    my $self = shift;
    $self->redirect_to( 'https://www.depesz.com/tag/paste-depesz-com/' );
}

1;
