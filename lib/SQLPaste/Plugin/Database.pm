package SQLPaste::Plugin::Database;

use Mojo::Base 'Mojolicious::Plugin';

use DBI;

has real_dbh        => undef;
has connection_args => sub { [] };
has log             => undef;

sub register {
    my ( $self, $app, $config ) = @_;

    # data source name
    my $dsn = $config->{ dsn };

    # if DSN not set directly
    unless ( $dsn ) {

        # driver
        my $driver = $config->{ driver } || 'Pg';

        # database name
        my $database = $config->{ database } || $self->moniker;

        # assemble
        my $dsn = sprintf 'dbi:%s:database=%s', $driver, $database;

        $dsn .= ';host=' . $config->{ host } if $config->{ host };
        $dsn .= ';port=' . $config->{ port } if $config->{ port };
    }

    # database (DBI) connection arguments
    $self->connection_args(
        [
            $config->{ dsn },
            $config->{ username },
            $config->{ password },
            $config->{ options } || {}
        ]
    );

    # log debug message
    $app->log->debug( 'Database connection args: ' . $app->dumper( $self->connection_args ) );
    $self->log( $app->log );

    # register helper
    $app->helper(
        database => sub {
            return $self;

        }
    );

    return;
}

sub dbh {
    my $self = shift;

    if (
        ($self->real_dbh) &&
        ($self->real_dbh->state =~ m{^(08|S8|57)})
    ) {
        $self->log->warn('DBH looks errored out, state ' . $self->real_dbh->state . ', reconnecting.' );
        $self->real_dbh(undef);
    }

    $self->real_dbh( DBI->connect( @{ $self->connection_args } ) ) unless $self->real_dbh;

    return $self->real_dbh;
}

sub get_pg_version {
    my $self = shift;
    return( ($self->dbh->selectrow_array('select format(?, pg_backend_pid(), version())', undef, '%s : %s'))[0] );
}

sub save_paste {
    my $self = shift;
    my ( $title, $content, $is_public, $expiry ) = @_;

    return $self->dbh->selectrow_arrayref(
        'SELECT register_paste(?, ?, ?, ?)',
        undef,
        $title,
        $content,
        $is_public,
        $expiry
    )->[0];
}

sub update_cache {
    my $self = shift;
    my ( $id, $cache ) = @_;
    my $data = $self->dbh->do(
        'UPDATE pastes SET cached = ? WHERE substr(id, 1,1) = ? and id = ?',
        undef,
        $cache,
        substr($id, 0, 1),
        $id,
    );
    return $data;
}

sub get_paste_data {
    my $self = shift;
    my $id = shift;
    my $data = $self->dbh->selectrow_hashref(
        'SELECT * FROM pastes WHERE substr(id, 1, 1) = ? AND id = ? AND not is_deleted',
        undef,
        substr($id, 0, 1),
        $id
    );
    return $data;
}

sub get_history_data {
    my $self = shift;
    my $data = $self->dbh->selectall_arrayref('
        SELECT
            id,
            title,
            to_char( entered_on, ?)
        FROM
            pastes
        WHERE
            is_public
            and not is_deleted
        ORDER BY
            entered_on DESC
        limit 100
        ',
        undef,
        'YYYY-MM-DD'
    );
    return $data;
}

sub DESTROY {
    my $self = shift;

    # nothing to do...
    return unless $self->dbh;

    # rollback uncommited transactions
    $self->dbh->rollback unless $self->connection_args->[ 3 ]->{ auto_commit };

    # disconnect
    $self->dbh->disconnect;

    $self->dbh( undef );

    return;
}

1;
