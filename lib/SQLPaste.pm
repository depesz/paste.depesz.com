package SQLPaste;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
    my $self = shift;

    # Add path to pgFormatter library, from git submodule
    push @INC, $self->home->rel_file( 'ext/pgFormatter/lib' )->to_string();

    # Mojolicious by default generates moniker s_q_l_paste, which I don't like
    $self->moniker( 'sqlpaste' );

    # Plugin to check if static file exists
    $self->plugin( 'SQLPaste::Plugin::StaticExists' );

    my $config = $self->plugin( 'JSONConfig' );

    $self->plugins->namespaces( [ "SQLPaste::Plugin", @{ $self->plugins->namespaces } ] );

    $self->plugin( 'database', $config->{ database } || {} );

    # Router
    my $r = $self->routes;

    # Welcome page
    $r->get( '/' )->to( 'controller#index' )->name( 'home' );
    $r->post( '/new-paste' )->to( 'controller#new_paste' )->name( 'new_paste' );
    $r->post( '/edit-new' )->to( 'controller#edit_new' )->name( 'edit_new' );
    $r->get( '/s/:id' )->to( 'controller#show', id => '' )->name( 'show' );
    $r->get( '/history' )->to( 'controller#history' )->name( 'history' );
    $r->get( '/about' )->to( 'controller#about' )->name( 'about' );
    $r->get( '/contact' )->to( 'controller#contact' )->name( 'contact' );

    # One-shot prettification of queries
    $r->post( '/prettify' )->to( 'controller#prettify' )->name( 'prettify' );
}

1;
