#!/usr/bin/env ruby
# encoding: utf-8

$LOAD_PATH.unshift File.realdirpath( File.join( File.dirname(__FILE__), '../ext/pg_query/lib' ) )
require 'pg_query'

puts PgQuery.parse( File.open( ARGV.first, 'r' ).read ).beautify
