# This file is used by cpanm.
# To install dependencies, run >> cpanm --local-lib ~/perl5 --installdeps . <<, or something similar, depending on your setup.
#
# More details:
# https://metacpan.org/pod/cpanfile
# https://metacpan.org/pod/cpanm

requires 'DBI';
requires 'DBD::Pg';
requires 'Mojolicious';
requires 'Pg::SQL::PrettyPrinter';

# vim: set ft=perl:
