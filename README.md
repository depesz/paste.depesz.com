paste.depesz.com
==================

This is code to serve paste site for SQL queries, with automatic re-formatting
for easier reading.

Setup
==================

First get the source code:

    $ git clone https://github.com/depesz/paste.depesz.com.git
    $ cd paste.depesz.com
    $ git submodule init
    $ git submodule update

Make sure you have following perl modules installed:

- Mojolicious
- DBI
- DBD::Pg

You can check if you have them installed by running:

    $ perl t/dependencies.t

If all lines (except for last one) start with "ok" then you should be good.

Afterwards, you need to create database for pastes, in PostgreSQL, of course.

To do it, in sql/ directory, run all scripts (and they should all work without
errors), with:

- first script (001.create.user.and.database.sql) should be ran as superuser
- all other scripts should be ran by depesz_paste account

With this in place, copy sqlpaste.development.json in top directory to
sqlpaste.production.json, and edit it so that it will have correct details
about how to connect to database.

Finally, in top directory, run:

    $ hypnotoad script/sqlpaste

and you should be set.
