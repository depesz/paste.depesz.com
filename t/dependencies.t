BEGIN {
    use Test::More;

    for my $module ( qw( Mojolicious DBI DBD::Pg ) ) {
        use_ok( $module );
    }

}

done_testing();
