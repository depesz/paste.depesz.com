BEGIN;
    SELECT _v.register_patch( '006.sample.query.forever', ARRAY[ '004.paste_table' ], NULL );

    INSERT INTO pastes.part_e (id, title, paste, is_public, entered_on, delete_after) VALUES (
        'e',
        'Optional title',
        'SELECT n.nspname as "Schema", c.relname as "Name", CASE c.relkind WHEN ''r'' THEN ''table'' WHEN ''v'' THEN ''view'' WHEN ''m'' THEN ''materialized view'' WHEN ''i'' THEN ''index'' WHEN ''S'' THEN ''sequence'' WHEN ''s'' THEN ''special'' WHEN ''f'' THEN ''foreign table'' WHEN ''P'' THEN ''table'' END as "Type", pg_catalog.pg_get_userbyid(c.relowner) as "Owner"
FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
WHERE c.relkind IN (''r'', ''P'',''v'',''m'',''S'',''f'','''') AND n.nspname <> ''pg_catalog'' AND n.nspname <> ''information_schema'' AND n.nspname !~ ''^pg_toast'' AND pg_catalog.pg_table_is_visible(c.oid)
ORDER BY 1,2;',
        true,
        now(),
        'infinity'
    );
commit;
