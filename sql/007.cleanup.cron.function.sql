BEGIN;
    SELECT _v.register_patch( '007.cleanup.cron.function', ARRAY[ '004.paste_table' ], NULL );

    do $$
    declare
        v_partition_list  TEXT[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
        v_partition_name  TEXT;
        v_current         TEXT;
        v_index_name      TEXT;
    BEGIN
        FOREACH v_current IN ARRAY v_partition_list
        LOOP
            v_partition_name := 'part_' || v_current;
            v_index_name := 'idx_' || v_current || '_for_delete';

            execute format(
                'CREATE INDEX %I ON pastes.%I (delete_after) WHERE NOT is_deleted',
                v_index_name,
                v_partition_name
            );

            raise notice 'Index % created.', v_index_name;
        END LOOP;
    END;
    $$ language plpgsql;

    CREATE OR REPLACE FUNCTION cron_paste_cleanup() RETURNS void as $$
    DECLARE
    BEGIN
        UPDATE pastes SET title = '', paste = '', is_deleted = true WHERE delete_after < now() AND NOT is_deleted;
        RETURN;
    END;
    $$ language plpgsql;

commit;
