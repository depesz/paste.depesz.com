BEGIN;
    SELECT _v.register_patch( '008.cache.for.reformatted', ARRAY[ '004.paste_table' ], NULL );

    ALTER TABLE pastes add column cached TEXT;

commit;
