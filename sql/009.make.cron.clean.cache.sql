BEGIN;
    SELECT _v.register_patch( '009.make.cron.clean.cache', ARRAY[ '008.cache.for.reformatted' ], NULL );

    CREATE OR REPLACE FUNCTION cron_paste_cleanup() RETURNS void as $$
    DECLARE
    BEGIN
        UPDATE pastes SET title = '', paste = '', is_deleted = true, cached = NULL WHERE delete_after < now() AND NOT is_deleted;
        RETURN;
    END;
    $$ language plpgsql;

    UPDATE pastes SET title = '', paste = '', cached = NULL WHERE is_deleted AND cached IS NOT NULL;

commit;
