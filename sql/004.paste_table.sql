BEGIN;
    SELECT _v.register_patch( '004.paste_table', ARRAY['003.short_ids.extension'], NULL );

    CREATE TABLE pastes (
        id            text                      NOT      NULL,
        paste         text                      NOT      NULL,
        entered_on    timestamp with time zone  NOT NULL  DEFAULT  now(),
        is_public     boolean                   NOT NULL  DEFAULT true,
        title         text,
        delete_after  timestamptz               NOT NULL,
        is_deleted    bool                      NOT NULL  DEFAULT false,
        PRIMARY KEY (id)
    );
    CREATE SCHEMA pastes;

    do $$
    declare
        v_partition_list  TEXT[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
        v_partition_name  TEXT;
        v_current         TEXT;
        v_sql             TEXT;
    BEGIN
        FOREACH v_current IN ARRAY v_partition_list
        LOOP
            v_partition_name := 'part_' || v_current;

            execute format(
                'CREATE TABLE pastes.%I (
                    PRIMARY KEY (id),
                    check ( substr(id, 1, 1) = %L )
                ) inherits (public.pastes)',
                v_partition_name,
                v_current
            );

            execute format(
                'CREATE INDEX %I ON pastes.%I (entered_on)',
                'idx_' || v_current || '_entered_on',
                v_partition_name
            );

            execute format(
                'CREATE INDEX %I ON pastes.%I (entered_on) WHERE is_public AND NOT is_deleted',
                'idx_' || v_current || '_entered_on_when_public',
                v_partition_name
            );
            raise notice 'Partition for %* created', v_current;
        END LOOP;
    END;
    $$ language plpgsql;

COMMIT;
