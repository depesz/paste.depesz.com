BEGIN;
    SELECT _v.register_patch( '005.paste.register.v1', ARRAY[ '004.paste_table' ], NULL );

CREATE OR REPLACE FUNCTION register_paste(
    in_title text,
    in_paste text,
    in_is_public boolean,
    in_keep_for interval
) RETURNS TEXT
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_hash_length INT4 := 2;
    v_id          TEXT;
    v_sql         TEXT;
BEGIN
    LOOP
        v_id := get_random_string( v_hash_length, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' );
        v_sql := format( 'INSERT INTO pastes.%I (id, title, paste, is_public, entered_on, delete_after) VALUES ($1, $2, $3, $4, now(), now() + $5 )', 'part_' || substr(v_id, 1, 1) );
        BEGIN
            execute v_sql using v_id, in_title, in_paste, in_is_public, in_keep_for;
            RETURN v_id;
        EXCEPTION WHEN unique_violation THEN
                -- do nothing
        END;
        v_hash_length := v_hash_length + 1;
        IF v_hash_length >= 30 THEN
            raise exception 'Random string of length == 30 requested. something is wrong.';
        END IF;
    END LOOP;
END;
$$;
COMMIT;
